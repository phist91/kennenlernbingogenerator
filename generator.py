
import random

def randomize_lines(input_file):
    # Read the lines from the input file
    with open(input_file, 'r') as f:
        lines = f.readlines()
        f.close

    # Randomly shuffle the lines
    random.shuffle(lines)

    return lines


def write_fields_to_file(fields, output_file):
     with open(output_file, 'w') as f:
        # Write each element of the array as a separate line
        for i in range(25):
            f.write('\\newcommand{\\feld' + int_to_letter(i) + '}{' + fields[i].strip() + '}\n')
        f.close   


def int_to_letter(n):
    if 0 <= n <= 25:
        return chr(ord('A') + n)
    else:
        raise ValueError("Input integer must be in the range 0 to 25")
    

def create_bingo(items_file, output_file):
    with open(output_file, 'w') as outfile:
        with open('./gen-preamble.tex','r') as pre:
            outfile.write(pre.read())
            pre.close()
        fields = randomize_lines(items_file)
        for i in range(25):
            outfile.write('\\newcommand{\\feld' + int_to_letter(i) + '}{' + fields[i].strip() + '}\n')
        with open('./gen-document.tex','r') as doc:
            outfile.write(doc.read())
            doc.close()
        outfile.close()

for i in range(30):
    create_bingo('./items.csv','./output/bingo-' + str(i) + '.tex')