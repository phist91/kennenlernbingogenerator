# Kennenlernbingogenerator

Dies ist ein Kennenlernbingogenerator. Er nutzt Python und LaTeX. Er funktioniert wie folgt:

- In der Datei `items.csv` mögliche Inhalte der Bingofelder eintragen.
- Gegebenenfalls im Python-Skript `generator.py` in Zeile 19 die Anzahl der zu erstellenden Bingo-Karten anpassen.
- Mittels `make` wird nun die gewünschte Menge an Bingo generiert: Dazu werden für jede Bingo-Karte 25 zufällige Einträge aus der `items.csv` gewählt und in eine LaTeX-Datei gestopft, die dann jeweils zu einer PDF kompiliert wird.
- Möchte man lieber nur die TeX-Dateien, aber keine PDFs haben, genügt ein `make tex`.
- `make clean` löscht temporäre LaTeX-Dateien, die beim Kompilieren entstanden sind. `make clean-hard` löscht auch die `.tex`- und `.pdf`-Dateien.

Das ganze funktioniert offenkundig nicht, wenn Python oder LaTeX nicht installiert sind.