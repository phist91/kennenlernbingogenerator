.PHONY: all tex pdf

all: pdf

tex:
	python3 generator.py

pdf: tex
	cd output && latexmk -pdf	

clean:
	cd output && latexmk -c

clean-hard:
	cd output && latexmk -C
	rm -f output/*.tex

